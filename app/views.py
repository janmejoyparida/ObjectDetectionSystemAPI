import flask
from flask import request, jsonify
import os
from app.imageutil import object_detect_from_image
from app.videoutils import object_detect_from_video, report_generate_video

UPLOAD_FOLDER = 'static/uploads'
REPORT_FOLDER = 'static/report'


def response_api(filename):
    path = os.path.join(REPORT_FOLDER,filename)
    f = open(path+".txt", "r")
    r = f.read()
    a: str = r
    if a.find('laptop') != -1:
        l = True
    else:
        l = False

    if a.find('person') != -1:
        p = True
    else:
        p = False

    if a.find('chair') != -1:
        c = True
    else:
        c = False

    if a.find('fire_extinguisher') != -1:
        d = True
    else:
        d = False

    result = {
        "Laptop Exist": l,
        "Person Exist": p,
        "Chair Exist": c,
        "Fire Extinguisher Exist": d
    }
    return jsonify(result)


def request_api():
    json_data = flask.request.json
    filename = json_data["filename"]
    if request.method == "GET":
        print(filename)
        if filename[-3:] == 'jpg':
            path = os.path.join(UPLOAD_FOLDER, filename)
            object_detect_from_image(path, filename, color='bgr')

        if filename[-3:] == 'mp4':
            path = os.path.join(UPLOAD_FOLDER, filename)
            object_detect_from_video(path, filename, color='bgr')
            report_generate_video(filename)

    return response_api(filename)
