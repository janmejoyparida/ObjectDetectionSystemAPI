from flask import Flask
from app import views

app = Flask(__name__)

# url
app.add_url_rule('/response_api','response_api',views.response_api)
app.add_url_rule('/request_api','request_api',views.request_api)
# 
if __name__ == "__main__":
    app.run(debug=True)